(hostname . "debian001")
(apt "packages-basic.list"
     "packages-nextcloud.list"
     "packages-editor.list"
     "packages-media.list"
     "packages-pub.list"
     "packages-sci.list"
     )
(skype . #f)
(flatpak "packages-flatpak.list")
(campusprint . #f)
(user . "lasse")
(sudo . #t)
(gsettings
 ("org.gnome.desktop.session" "idle-delay" . 900) ; blank screen
 ("org.gnome.desktop.screensaver" "lock-delay" . 1800) ; lock screen
 ("org.gnome.settings-daemon.plugins.power" "sleep-inactive-battery-timeout" . 3600)
 ("org.gnome.settings-daemon.plugins.power" "sleep-inactive-ac-timeout" . 7200)
 ("org.gnome.desktop.peripherals.touchpad" "tap-to-click" . "true")
 ("org.gnome.desktop.wm.preferences" "button-layout" . "appmenu:minimize,close")
 ("org.gnome.desktop.input-sources" "sources"
  "('xkb', 'spaceJ')"
  "('xkb', 'de')"
  "('xkb', 'us')")
 ("org.gnome.shell" "favorite-apps"
  "org.remmina.Remmina.desktop"
  "chromium.desktop"
  "firefox-esr.desktop"
  "org.gnome.Nautilus.desktop"
  "org.gnome.Rhythmbox3.desktop"
  "pavucontrol.desktop"
  "us.zoom.Zoom.desktop"
  "com.obsproject.Studio.desktop"
  "org.gnome.Terminal.desktop"
  ))
(off-units "mono-xsp4.service" "sshd.service")
(spaceJ . #f)
(timeserver "ts1.rz.uni-kiel.de" "ts2.rz.uni-kiel.de" "0.pool.ntp.org" "1.pool.ntp.org" "2.pool.ntp.org")
(relayhost . "[snyder.systems]:10025")
(admin-email . "lasse@lassekliemann.de")
