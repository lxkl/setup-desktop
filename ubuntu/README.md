# Ubuntu

* Install, including third-party software.
* Skip livepatch.
* "Software & Updates" --> Check if security updates are set to "Download and install automatically"
* If there are updates, have them installed, reboot.
* Config printer: `./config-printer deb sunmsXXX`
* Setup firewall: `./setup-firewall`
* Setup packages: `./setup-packages`
* Setup modules: `./setup-modules`
* Enable UA.
* Reboot.
